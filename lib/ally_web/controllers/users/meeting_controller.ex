defmodule AllyWeb.Users.MeetingController do
  use AllyWeb, :controller

  alias Ally.Users

  alias Ally.Meeting.{
    Instance,
    Participant,
    Request
  }

  action_fallback AllyWeb.FallbackController

  # def index(conn, params) do

  #   meetings = Users.list_meetings("")
  #   render(conn, "index.json", meetings: meetings)
  # end

  @spec create(conn :: Plug.Conn.t(), request :: map()) :: Plug.Conn.t()
  def create(conn, %{"meeting" => meeting_params}) do
    meeting_request =
      %Request{}
      |> Request.changeset(meeting_params)
      |> Ecto.Changeset.apply_action!(:update)

    case Ally.request_meeting(meeting_params["owner_id"], meeting_request) do
      {:ok, %Instance{} = meeting} ->
        meeting = Ally.Repo.preload(meeting, :room_instance)

        conn
        |> put_status(:created)
        |> put_resp_header("location", Routes.users_meeting_path(conn, :show, meeting))
        |> render("show.json", meeting: meeting)

      _error ->
        # TODO: Gracefully handle error case with {:error, :no_rooms}
        conn
    end
  end

  @spec show(conn :: Plug.Conn.t(), attrs :: map()) :: Plug.Conn.t()
  def show(conn, %{"id" => id}) do
    # meeting = Users.get_meeting!(id)
    # render(conn, "show.json", meeting: meeting)users
    meetings = Users.list_meetings(id)
    render(conn, "index.json", meetings: meetings)
  end

  # @spec update(conn :: Plug.Conn.t(), attrs :: map()) :: Plug.Conn.t()
  # def update(conn, %{"id" => id, "meeting" => meeting_params}) do
  # meeting = Users.get_meeting!(id)

  # with {:ok, %Meeting{} = meeting} <- Users.update_meeting(meeting, meeting_params) do
  #   render(conn, "show.json", meeting: meeting)
  # end
  # end

  # def delete(conn, %{"id" => id}) do
  #   # meeting = Users.get_meeting!(id)

  #   # with {:ok, %Meeting{}} <- Users.delete_meeting(meeting) do
  #   #   send_resp(conn, :no_content, "")
  #   # end
  # end
end
