defmodule AllyWeb.PageController do
  use AllyWeb, :controller

  @spec index(conn :: Plug.Conn.t(), params :: any()) :: Plug.Conn.t()
  def index(conn, _params) do
    render(conn, "index.html")
  end
end
