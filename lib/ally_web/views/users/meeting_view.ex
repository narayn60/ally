defmodule AllyWeb.Users.MeetingView do
  use AllyWeb, :view
  alias AllyWeb.Users.MeetingView

  @impl Phoenix.View
  def render("index.json", %{meetings: meetings}) do
    %{data: render_many(meetings, MeetingView, "meeting.json")}
  end

  @impl Phoenix.View
  def render("show.json", %{meeting: meeting}) do
    %{data: render_one(meeting, MeetingView, "meeting.json")}
  end

  @impl Phoenix.View
  def render("meeting.json", %{meeting: meeting}) do
    %{
      id: meeting.id,
      name: meeting.name,
      beforeTime: meeting.before_time,
      afterTime: meeting.after_time,
      startTime: meeting.range.first,
      endTime: meeting.range.last,
      room: meeting.room_instance.name
    }
  end
end
