defmodule Ally do
  @moduledoc false

  alias Ally.{
    Meeting,
    Repo,
    Room
  }

  @spec request_meeting(
          owner_id :: Ally.Owner.owner_instance_id(),
          meeting_request :: %Meeting.Request{}
        ) ::
          {:ok, %Meeting.Instance{}} | {:error, Ecto.Changeset.t()} | {:error, :no_rooms}
  def request_meeting(
        owner_id,
        %Meeting.Request{
          name: name,
          after_time: after_time,
          before_time: before_time,
          user_emails: user_emails,
          duration_in_minutes: duration_in_minutes
        }
      ) do
    # TODO: MAKE THIS A TRANSACTION
    Ecto.Multi.new()
    |> Ecto.Multi.run(:meeting_slot, fn _repo, _changes ->
      case Ally.Filter.find_earliest_available(
             owner_id,
             Timestamp.Range.new(after_time, before_time),
             length(user_emails),
             duration_in_minutes * 60,
             %{hour: 9, minute: 0},
             %{hour: 17, minute: 0}
           ) do
        nil -> {:error, :no_rooms}
        meeting_slot -> {:ok, meeting_slot}
      end
    end)
    |> Ecto.Multi.run(:meeting, fn _repo,
                                   %{
                                     meeting_slot: %{
                                       meeting_slot: time_range,
                                       room_id: room_id
                                     }
                                   } ->
      {:ok, time_range} = Timestamp.Range.load(time_range)

      Meeting.create(
        %{
          name: name,
          range: time_range,
          after_time: after_time,
          before_time: before_time,
          room_instance_id: room_id
        },
        user_emails
      )
    end)
    |> Repo.transaction()
    |> case do
      {:ok, %{meeting: meeting}} ->
        {:ok, meeting}

      {:error, :meeting_slot, :no_rooms, _} ->
        {:error, :no_rooms}

      error ->
        # TODO: DEAL WITH OTHER ERROR CASE
        error
    end
  end

  @spec update_meeting(
          owner_id :: Ally.Owner.owner_instance_id(),
          meeting_request :: %Meeting.Request{},
          current_meeting_id :: Meeting.Instance.id()
        ) ::
          {:ok, %Meeting.Instance{}}
          | {:error, :invalid_meeting_id}
          | {:error, Ecto.Changeset.t()}
  def update_meeting(owner_id, meeting_request, current_meeting_id) do
    case Meeting.get(current_meeting_id) do
      %Meeting.Instance{} = current_meeting ->
        update_room(
          meeting_request,
          current_meeting_id,
          Room.query_available(owner_id, meeting_request, [current_meeting])
        )

      nil ->
        {:error, :invalid_meeting_id}
    end
  end

  @spec delete_meeting(
          owner_id :: Ally.Owner.owner_instance_id(),
          meeting_id :: Meeting.Instance.id()
        ) ::
          {:ok, %Meeting.Instance{}} | {:error, String.t()}
  def delete_meeting(owner_id, meeting_id) do
    case Meeting.delete(meeting_id) do
      {:ok, %Meeting.Instance{range: range}} = ok_response ->
        run_optimisation(owner_id, range)
        ok_response

      err ->
        err
    end
  end

  @spec update_room(
          meeting_request :: %Meeting.Request{},
          meeting_id :: String.t(),
          room_instance :: %Room.Instance{} | nil
        ) :: {:ok, %Meeting.Instance{}} | {:error, Ecto.Changeset.t()} | {:error, :no_rooms}
  defp update_room(_meeting_request, _meeting_id, nil), do: {:error, :no_rooms}

  defp update_room(
         %Ally.Meeting.Request{
           name: name,
           after_time: start_time,
           before_time: end_time,
           user_emails: user_emails
         },
         meeting_id,
         %Ally.Room.Instance{id: room_instance_id}
       ) do
    meeting_range = Timestamp.Range.new(start_time, end_time)

    Meeting.update(meeting_id, %{
      name: name,
      range: meeting_range,
      after_time: start_time,
      before_time: end_time,
      room_instance_id: room_instance_id,
      user_emails: user_emails
    })
  end

  @spec run_optimisation(owner_id :: Ally.Owner.owner_instance_id(), range :: %Timestamp.Range{}) ::
          :ok
  def run_optimisation(owner_id, range) do
    {:ok, postgrex_range} = Timestamp.Range.dump(range)
    meetings = Meeting.get_in_range_ordered_by_participants(owner_id, postgrex_range)
    do_run_optimisation(owner_id, meetings)
  end

  @spec do_run_optimisation(
          owner_id :: Ally.Owner.owner_instance_id(),
          meeting_instances :: [%Meeting.Instance{}]
        ) :: :ok
  defp do_run_optimisation(_owner_id, []), do: :ok

  defp do_run_optimisation(
         owner_id,
         [
           %Meeting.Instance{
             id: meeting_id,
             name: name,
             range: %Timestamp.Range{first: first, last: last} = range
           }
           | other_meetings
         ] = remaining_meetings
       ) do
    user_emails = Meeting.get_participants_emails(meeting_id)

    %Room.Instance{id: room_instance_id} =
      Room.query_available(
        owner_id,
        %Meeting.Request{
          name: name,
          user_emails: user_emails,
          after_time: first,
          before_time: last
        },
        remaining_meetings
      )

    Meeting.update(meeting_id, %{
      name: name,
      range: range,
      room_instance_id: room_instance_id,
      user_emails: user_emails
    })

    do_run_optimisation(owner_id, other_meetings)
  end
end
