defmodule Ally.User do
  @moduledoc false

  alias Ally.Repo
  alias Ally.User.Instance

  @type id :: String.t()
  @type repo_return :: {:ok, %Instance{}} | {:error, Ecto.Changeset.t()}

  @spec create(user :: map()) :: repo_return
  def create(user) do
    %Instance{}
    |> Instance.changeset(user)
    |> Repo.insert()
  end

  @spec get(user_id :: id()) :: %Instance{} | nil
  def get(user_id), do: Repo.get(Instance, user_id)
end
