defmodule Ally.User.Instance do
  @moduledoc false

  use Ally.Schema
  import Ecto.Changeset

  @type email :: String.t()

  schema "user_instances" do
    field :email, :string
    field :name, :string
    field :password, :string

    has_many :event_instances, Ally.Event.Instance, foreign_key: :id, on_delete: :delete_all

    timestamps()
  end

  @doc false
  @spec changeset(instance :: %Ally.User.Instance{}, attrs :: map()) :: Ecto.Changeset.t()
  def changeset(instance, attrs) do
    instance
    |> cast(attrs, [:name, :email, :password])
    |> validate_required([:name, :email, :password])
    |> unique_constraint(:email)
  end
end
