defmodule Ally.Meeting.Instance do
  @moduledoc false

  @type id :: String.t()

  use Ally.Schema

  import Ecto.Changeset

  schema "meeting_instances" do
    field :name, :string
    field :range, Timestamp.Range
    field :before_time, :naive_datetime
    field :after_time, :naive_datetime

    belongs_to :room_instance, Ally.Room.Instance

    has_many :event_instances, Ally.Event.Instance, foreign_key: :id, on_delete: :delete_all

    timestamps()
  end

  @doc false
  @spec changeset(instance :: %Ally.Meeting.Instance{}, attrs :: map()) :: Ecto.Changeset.t()
  def changeset(instance, attrs) do
    instance
    |> cast(attrs, [:name, :range, :room_instance_id, :before_time, :after_time])
    |> validate_required([:name, :range, :room_instance_id, :before_time, :after_time])
    |> assoc_constraint(:room_instance)
  end
end
