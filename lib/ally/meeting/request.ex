defmodule Ally.Meeting.Request do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  @fields [:name, :before_time, :after_time, :duration_in_minutes, :user_emails]

  embedded_schema do
    field :name, :string
    field :before_time, :naive_datetime
    field :after_time, :naive_datetime
    field :duration_in_minutes, :integer
    field :user_emails, {:array, :string}
  end

  @spec changeset(request :: %Ally.Meeting.Request{}, attrs :: map()) :: Ecto.Changeset.t()
  def changeset(%Ally.Meeting.Request{} = request, attrs) do
    request
    |> cast(attrs, @fields)
    |> validate_required(@fields)
  end
end
