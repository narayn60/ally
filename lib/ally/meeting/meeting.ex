defmodule Ally.Meeting do
  @moduledoc false

  import Ecto.Query

  alias Ally.Meeting.Instance

  alias Ally.Repo

  @spec create(meeting_attrs :: map(), user_emails :: [String.t()]) ::
          {:ok, %Instance{}} | {:error, Ecto.Changeset.t()}
  def create(meeting_attrs, user_emails) do
    meeting_changeset = Instance.changeset(%Instance{}, meeting_attrs)

    # TODO: Ensure that these users exist
    # TODO: Remove participant totally, and replace directly with user instance
    users =
      Ally.User.Instance
      |> where([u], u.email in ^user_emails)
      |> Repo.all()

    Ecto.Multi.new()
    |> Ecto.Multi.insert(:meeting, meeting_changeset)
    |> Ecto.Multi.insert_all(:events, Ally.Event.Instance, fn %{
                                                                meeting: %Instance{id: meeting_id}
                                                              } ->
      Enum.map(users, &%{user_id: &1.id, meeting_id: meeting_id})
    end)
    |> Repo.transaction()
    |> case do
      {:ok, %{meeting: meeting}} -> {:ok, meeting}
    end
  end

  @spec delete(id :: Instance.id()) :: {:ok, %Instance{}} | {:error, String.t()}
  def delete(id) do
    case Repo.get(Instance, id) do
      %Instance{} = instance -> Repo.delete(instance)
      nil -> {:error, "Meeting instance with id #{id} doesn't exist"}
    end
  end

  @spec update(id :: String.t(), meeting :: map()) ::
          {:ok, %Instance{}} | {:error, Ecto.Changeset.t()} | {:error, :not_found}
  def update(id, meeting) do
    case Repo.get(Instance, id) do
      %Instance{} = instance -> instance |> Instance.changeset(meeting) |> Repo.update()
      nil -> {:error, :not_found}
    end
  end

  @spec get(id :: Instance.id()) :: %Instance{} | nil
  def get(id), do: Repo.get(Instance, id)

  @spec get_participants_emails(id :: Instance.id()) :: [String.t()]
  def get_participants_emails(id) do
    Instance
    |> where([m], m.id == ^id)
    |> join(:inner, [m], e in Ally.Event.Instance, on: e.meeting_id == m.id, as: :event_instance)
    |> join(:inner, [event_instance: e], assoc(e, :user), as: :user)
    |> select([user: u], u.email)
    |> Repo.all()
  end

  @spec get_in_range_ordered_by_participants(
          owner_id :: Ally.Owner.owner_instance_id(),
          range :: %Postgrex.Range{}
        ) :: [
          %Instance{}
        ]
  def get_in_range_ordered_by_participants(owner_id, range) do
    Instance
    |> join(:inner, [m], assoc(m, :room_instance), as: :room_instance)
    |> where([room_instance: r], r.owner_instance_id == ^owner_id)
    |> join(:inner, [m], e in Ally.Event.Instance, on: e.meeting_id == m.id, as: :event_instance)
    |> where([m], fragment("?::tsrange && ?", ^range, m.range))
    |> group_by([m], m.id)
    |> select([m], m)
    |> order_by([event_instance: e], asc: count(e.id))
    |> Repo.all()
  end
end
