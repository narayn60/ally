defmodule Ally.Repo do
  use Ecto.Repo,
    otp_app: :ally,
    adapter: Ecto.Adapters.Postgres
end
