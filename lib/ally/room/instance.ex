defmodule Ally.Room.Instance do
  @moduledoc false

  use Ally.Schema
  import Ecto.Changeset

  @type id :: String.t()

  schema "room_instances" do
    field :name, :string
    field :size, :integer

    belongs_to :owner_instance, Ally.Owner.Instance

    has_many :meeting_instances, Ally.Meeting.Instance, foreign_key: :id, on_delete: :delete_all

    timestamps()
  end

  @spec changeset(instance :: %Ally.Room.Instance{}, attrs :: map()) :: Ecto.Changeset.t()
  def changeset(%Ally.Room.Instance{} = instance, attrs) do
    instance
    |> cast(attrs, [:size, :name, :owner_instance_id])
    |> validate_required([:size, :name, :owner_instance_id])
    |> assoc_constraint(:owner_instance)
  end
end
