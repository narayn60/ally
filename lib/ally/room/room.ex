defmodule Ally.Room do
  @moduledoc false

  alias Ally.Meeting.Request
  alias Ally.Repo
  alias Ally.Room.Instance

  import Ecto.Query

  @type instance_id :: String.t()
  @type repo_return :: {:ok, %Instance{}} | {:error, Ecto.Changeset.t()}
  @type owner_id :: String.t()

  @spec create(owner_id :: owner_id, room :: map()) ::
          {:ok, %Instance{}} | {:error, Ecto.Changeset.t()}
  def create(owner_id, room) do
    %Instance{}
    |> Instance.changeset(Map.put(room, :owner_instance_id, owner_id))
    |> Repo.insert()
  end

  @spec delete(instance_id :: instance_id) :: repo_return | {:error, String.t()}
  def delete(instance_id) when is_binary(instance_id),
    do: instance_update(instance_id, &Repo.delete(&1))

  @spec update(instance_id :: instance_id, updates :: map()) ::
          repo_return | {:error, String.t()}
  def update(instance_id, updates) when is_binary(instance_id),
    do: instance_update(instance_id, &(&1 |> Instance.changeset(updates) |> Repo.update()))

  @spec get_all(owner_id :: owner_id) :: [%Instance{}]
  def get_all(owner_id),
    do:
      Instance
      |> where([r], r.owner_instance_id == ^owner_id)
      |> Repo.all()

  @spec get(room_id :: String.t()) :: %Instance{} | nil
  def get(room_id), do: Repo.get(Instance, room_id)

  # def find_earliest_available_room(
  #       _user_id,
  #       %Request{
  #         participants: participants,
  #         after_time: start_time,
  #         before_time: end_time,
  #         duration_in_minutes: duration_in_minutes
  #       }
  #     ) do
  # end

  @doc """
  Returns if any rooms are available that meet the specifications of the request. Returns
  the room with the optimal size, otherwise returns nil.
  """
  @spec query_available(
          owner_id :: owner_id,
          meeting_request :: %Ally.Meeting.Request{},
          excluded_meetings :: [%Ally.Meeting.Instance{}]
        ) ::
          %Instance{} | nil
  def query_available(
        owner_id,
        %Request{
          user_emails: participants,
          after_time: start_time,
          before_time: end_time
        },
        excluded_meetings \\ []
      ) do
    no_participants = length(participants)

    Ally.Filter.get_room_within_range_with_participants(
      owner_id,
      Timestamp.Range.new(start_time, end_time),
      no_participants,
      excluded_meetings
    )
  end

  @spec instance_update(instance_id :: instance_id, success_fun :: (instance_id -> repo_return())) ::
          repo_return() | {:error, String.t()}
  defp instance_update(instance_id, success_fun) do
    case get(instance_id) do
      %Instance{} = instance -> success_fun.(instance)
      nil -> {:error, "Instance with id #{instance_id} doesn't exist"}
    end
  end
end
