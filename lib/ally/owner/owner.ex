defmodule Ally.Owner do
  @moduledoc false

  alias Ally.Owner.Instance
  alias Ally.Repo

  @type owner_instance_id :: String.t()
  @type repo_return :: {:ok, %Instance{}} | {:error, Ecto.Changeset.t()}

  @spec create(user_details :: map()) :: repo_return
  def create(user_details) do
    %Instance{}
    |> Instance.changeset(user_details)
    |> Repo.insert()
  end

  @spec delete(owner_id :: owner_instance_id) :: repo_return
  def delete(owner_id) when is_binary(owner_id),
    do: instance_update(owner_id, &Repo.delete(&1))

  @spec get(owner_id :: owner_instance_id) :: %Instance{} | nil
  def get(owner_id), do: Repo.get(Instance, owner_id)

  @spec instance_update(
          owner_id :: owner_instance_id,
          success_fun :: (owner_instance_id -> repo_return)
        ) ::
          repo_return() | {:error, String.t()}
  defp instance_update(owner_id, success_fun) do
    case get(owner_id) do
      %Instance{} = instance -> success_fun.(instance)
      nil -> {:error, "Instance with id #{owner_id} doesn't exist"}
    end
  end
end
