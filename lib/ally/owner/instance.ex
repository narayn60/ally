defmodule Ally.Owner.Instance do
  @moduledoc false

  use Ally.Schema
  import Ecto.Changeset

  schema "owner_instances" do
    field :email, :string

    has_many :room_instances, Ally.Room.Instance, foreign_key: :id

    timestamps()
  end

  @spec changeset(instance :: %Ally.Owner.Instance{}, attrs :: map()) :: Ecto.Changeset.t()
  def changeset(instance, attrs) do
    instance
    |> cast(attrs, [:email])
    |> validate_required([:email])
    |> unique_constraint(:email)
  end
end
