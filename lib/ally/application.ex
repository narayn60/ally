defmodule Ally.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @spec start(type :: any(), args :: Keyword.t()) ::
          {:ok, pid} | {:error, {:already_started, pid} | {:shutdown, term} | term}
  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Ally.Repo,
      # Start the Telemetry supervisor
      AllyWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Ally.PubSub},
      # Start the Endpoint (http/https)
      AllyWeb.Endpoint
      # Start a worker by calling: Ally.Worker.start_link(arg)
      # {Ally.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Ally.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @spec config_change(changed :: any(), new :: any(), removed :: any()) :: :ok
  def config_change(changed, _new, removed) do
    AllyWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
