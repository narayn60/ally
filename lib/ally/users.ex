defmodule Ally.Users do
  @moduledoc """
  The Users context.
  """

  import Ecto.Query, warn: false
  alias Ally.Repo

  # alias Ally.Meeting
  alias Ally.Meeting.Instance, as: MeetingInstance
  alias Ally.Meeting.Request
  alias Ally.User.Instance, as: UserInstance

  @spec list_meetings(user_id :: String.t()) :: [%MeetingInstance{}]
  def list_meetings(user_id) do
    meetings =
      Ally.Event.Instance
      |> where([e], e.user_id == ^user_id)
      |> join(:inner, [e], m in MeetingInstance,
        on: e.meeting_id == m.id,
        as: :meeting_instance
      )
      |> select([meeting_instance: m], m)
      |> Repo.all()

    # TODO: Move this into the query above somehow
    Enum.map(meetings, &Ally.Repo.preload(&1, :room_instance))
  end

  @spec get_meeting!(id :: String.t()) :: %MeetingInstance{}
  def get_meeting!(id), do: Repo.get!(MeetingInstance, id)

  @spec create_meeting(owner_id :: String.t(), meeting_request :: %Request{}) ::
          {:error, :no_rooms} | {:ok, %MeetingInstance{}}
  def create_meeting(owner_id, meeting_request) do
    Ally.request_meeting(owner_id, meeting_request)
  end

  # def update_meeting(%Instance{} = meeting, attrs) do
  # meeting
  # |> Instance.changeset(attrs)
  # |> Repo.update()
  # end

  # @spec delete_meeting(instance :: %Ally.Meeting.Instance{}) :: nil
  # def delete_meeting(%Instance{} = meeting) do
  #   # Repo.delete(meeting)
  # end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking meeting changes.

  ## Examples

      iex> change_meeting(meeting)
      %Ecto.Changeset{data: %Instance{}}

  """
  # def change_meeting(%Instance{} = meeting, _attrs \\ %{}) do
  # Meeting.changeset(meeting, attrs)
  # end
end
