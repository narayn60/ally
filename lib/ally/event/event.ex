defmodule Ally.Event do
  @moduledoc false

  alias Ally.Event.Instance
  alias Ally.Repo

  @type id :: String.t()
  @type repo_return :: {:ok, %Instance{}} | {:error, Ecto.Changeset.t()}

  @spec create(user_id :: Ally.User.id(), meeting_id :: Ally.Meeting.Instance.id()) ::
          repo_return()
  def create(user_id, meeting_id) do
    %Instance{}
    |> Instance.changeset(%{"user_id" => user_id, "meeting_id" => meeting_id})
    |> Repo.insert()
  end

  @spec get(event_id :: id()) :: %Instance{} | nil
  def get(event_id), do: Repo.get(Instance, event_id)
end
