defmodule Ally.Event.Instance do
  @moduledoc false

  use Ally.Schema
  import Ecto.Changeset

  schema "event_instances" do
    field :attending, :boolean, default: true

    belongs_to :user, Ally.User.Instance
    belongs_to :meeting, Ally.Meeting.Instance

    timestamps()
  end

  @doc false
  @spec changeset(instance :: %Ally.Event.Instance{}, attrs :: map()) :: Ecto.Changeset.t()
  def changeset(instance, attrs) do
    instance
    |> cast(attrs, [:user_id, :meeting_id, :attending])
    |> validate_required([:user_id, :meeting_id])
    |> assoc_constraint(:user)
    |> assoc_constraint(:meeting)
  end
end
