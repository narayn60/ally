defmodule Ally.Filter do
  @moduledoc """
  This module defines different ways of filtering rooms
  """

  import Ecto.Query

  @doc """
  Returns a `Ally.Room.Instance.t()` if a room is available for the given time
  range with the given number of participants. Otherwise returns `nil`.
  """
  @spec get_room_within_range_with_participants(
          owner_id :: Ally.Owner.owner_instance_id(),
          time_range :: %Timestamp.Range{},
          no_participants :: number(),
          excluded_meetings :: [%Ally.Meeting.Instance{}]
        ) :: %Ally.Room.Instance{} | nil
  def get_room_within_range_with_participants(
        owner_id,
        time_range,
        no_participants,
        excluded_meetings
      ) do
    excluded_meetings_ids = Enum.map(excluded_meetings, & &1.id)
    booked_rooms = booked_rooms(owner_id, time_range, excluded_meetings_ids)

    applicable_rooms = applicable_rooms(owner_id, no_participants)

    applicable_rooms
    |> except(^booked_rooms)
    |> subquery()
    |> from(order_by: [asc: :size])
    |> limit(1)
    |> Ally.Repo.one()
  end

  @spec get_all_booked_rooms(
          owner_id :: Ally.Owner.owner_instance_id(),
          meeting_range :: %Timestamp.Range{},
          excluded_meetings_ids :: [String.t()]
        ) :: [%Ally.Room.Instance{}]
  def get_all_booked_rooms(owner_id, meeting_range, excluded_meetings_ids \\ []) do
    owner_id
    |> booked_rooms(meeting_range, excluded_meetings_ids)
    |> subquery()
    |> from(order_by: [asc: :size])
    |> Ally.Repo.all()
  end

  @spec booked_rooms(
          owner_id :: Ally.Owner.owner_instance_id(),
          meeting_range :: %Timestamp.Range{},
          excluded_meetings_ids :: [String.t()]
        ) :: %Ecto.Query{}
  defp booked_rooms(owner_id, meeting_range, excluded_meetings_ids) do
    {:ok, postgrex_range} = Timestamp.Range.dump(meeting_range)

    Ally.Room.Instance
    |> where([r], r.owner_instance_id == ^owner_id)
    |> join(:inner, [r], m in Ally.Meeting.Instance,
      on:
        fragment("?::tsrange && ?", ^postgrex_range, m.range) and
          m.id not in ^excluded_meetings_ids and
          m.room_instance_id == r.id
    )
  end

  @doc """
  Returns a map where the key is the room id and the value is the available times
  """
  @spec find_available_rooms(
          owner_id :: Ally.Owner.owner_instance_id(),
          time_range :: %Timestamp.Range{},
          no_participants :: integer(),
          duration_in_seconds :: integer(),
          business_start_time :: %{hour: integer(), minute: integer()},
          business_end_time :: %{hour: integer(), minute: integer()}
        ) :: Ecto.Query.t()
  def find_available_rooms(
        owner_id,
        # period we're searching in
        time_range,
        no_participants,
        duration_in_seconds,
        %{hour: business_start_hour, minute: business_start_minute} = _business_start_time,
        %{hour: business_end_hour, minute: business_end_minute} = _business_end_time
      ) do
    {:ok, search_range} = Timestamp.Range.dump(time_range)

    business_start = %{
      time_range.first
      | hour: business_start_hour,
        minute: business_start_minute
    }

    business_end = %{time_range.first | hour: business_end_hour, minute: business_end_minute}
    business_end_of_day_during_search_range = %{time_range.last | hour: 23, minute: 59}

    # How long the meeting lasts
    duration = %Postgrex.Interval{secs: duration_in_seconds}

    # Set all out of business hours as meetings and union with initial and end search:vs

    query =
      from r in applicable_rooms(owner_id, no_participants),
        cross_join:
          day in fragment(
            "((?) UNION (?))",
            fragment(
              "SELECT a, a + ((?::timestamp + INTERVAL '1 day') - ?::timestamp) as b FROM generate_series(?::timestamp, ?::timestamp, '1 day') a",
              # Monday 09:00
              ^business_start,
              # Friday 18:00
              ^business_end,
              # Friday 18:00
              ^business_end,
              # Friday 23:59
              ^business_end_of_day_during_search_range
            ),
            fragment(
              "SELECT * FROM (VALUES (lower(?::tsrange) - INTERVAL '1 day', lower(?::tsrange)), (upper(?::tsrange), upper(?::tsrange) + INTERVAL '1 day')) t1 (a, b)",
              ^search_range,
              ^search_range,
              ^search_range,
              ^search_range
            )
          ),
        order_by: day.a,
        select: %{
          room_id: r.id,
          room_size: r.size,
          meeting_range: fragment("tsrange(?, ?)", day.a, day.b)
        }

    # Get the current meetings
    query =
      from r in applicable_rooms(owner_id, no_participants),
        left_join: m in Ally.Meeting.Instance,
        on: m.room_instance_id == r.id,
        # TODO: Make this an inclusive check
        where: fragment("?::tsrange && ?", ^search_range, m.range) or is_nil(m),
        select: %{
          room_id: r.id,
          room_size: r.size,
          meeting_range: m.range
        },
        union: ^query

    # Get all ranges
    query =
      from r in subquery(query),
        windows: [w: [order_by: fragment("lower(?)", r.meeting_range), partition_by: r.room_id]],
        select: %{
          room_id: r.room_id,
          room_size: r.room_size,
          start_time: fragment("lower(?)", r.meeting_range),
          diff:
            "lower(?) - lag(upper(?))" |> fragment(r.meeting_range, r.meeting_range) |> over(:w)
        }

    # Caclculate the diffs between all ranges and extract those that are greter than the given duration
    from e in subquery(query),
      where: e.diff >= ^duration,
      select: %{
        room_id: e.room_id,
        room_size: e.room_size,
        time_range: fragment("tsrange(? - ?, ?)", e.start_time, e.diff, e.start_time)
      }
  end

  @spec find_earliest_available(
          owner_id :: Ally.Owner.owner_instance_id(),
          time_range :: %Timestamp.Range{},
          no_participants :: integer(),
          duration_in_seconds :: integer(),
          business_start_time :: %{hour: integer(), minute: integer()},
          business_end_time :: %{hour: integer(), minute: integer()}
        ) ::
          nil
          | %{
              room_id: Ally.Room.instance_id(),
              room_size: integer(),
              time_range: %Postgrex.Range{}
            }
  def find_earliest_available(
        owner_id,
        time_range,
        no_participants,
        duration_in_seconds,
        business_start_time,
        business_end_time
      ) do
    # How long the meeting lasts
    duration = %Postgrex.Interval{secs: duration_in_seconds}

    order_by = [
      asc: :room_size,
      asc: dynamic([r], fragment("lower(?)", r.time_range))
    ]

    query =
      from r in subquery(
             find_available_rooms(
               owner_id,
               time_range,
               no_participants,
               duration_in_seconds,
               business_start_time,
               business_end_time
             )
           ),
           group_by: [:room_id, :time_range, :room_size],
           order_by: ^order_by,
           limit: 1,
           select: %{
             room_id: r.room_id,
             room_size: r.room_size,
             meeting_slot:
               fragment(
                 "tsrange(lower(?::tsrange), lower(?::tsrange) + ?)",
                 r.time_range,
                 r.time_range,
                 ^duration
               )
           }

    Ally.Repo.one(query)
  end

  @spec applicable_rooms(owner_id :: Ally.Owner.owner_instance_id(), no_participants :: number()) ::
          Ecto.Query.t()
  def applicable_rooms(owner_id, no_participants) do
    Ally.Room.Instance
    |> where([r], r.owner_instance_id == ^owner_id)
    |> where([r], r.size >= ^no_participants)
  end
end
