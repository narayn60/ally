defmodule Ally.CalendarCases.EventUpdateTest do
  @moduledoc false

  use ExUnit.Case

  alias Ecto.Adapters.SQL.Sandbox
  alias Ally.{Meeting, Room}

  setup do
    :ok = Sandbox.checkout(Ally.Repo)

    owner_id = TestFactory.create_owner()

    [room_1, room_2, room_3] =
      Enum.map(
        [
          %{size: 3, name: "room_1"},
          %{size: 5, name: "room_2"},
          %{size: 6, name: "room_3"}
        ],
        fn args ->
          {:ok, room} = Ally.Room.create(owner_id, args)
          room
        end
      )

    [room_1: room_1, room_2: room_2, room_3: room_3, owner_id: owner_id]
  end

  describe "Event Update" do
    test "Can successfully update the number of participants", %{
      room_1: room_1,
      room_2: room_2,
      owner_id: owner_id
    } do
      start_time = ~N[2020-01-01 14:00:00]
      end_time = ~N[2020-01-01 15:00:00]

      {:ok, %Meeting.Instance{id: meeting_id, room_instance_id: room_instance_id}} =
        Ally.Meeting.create(
          %{
            name: "meeting_1",
            range: Timestamp.Range.new(start_time, end_time),
            after_time: start_time,
            before_time: end_time,
            room_instance_id: room_1.id
          },
          TestFactory.participant_gen(3)
        )

      assert ^room_1 = Room.get(room_instance_id)

      {:ok, %Meeting.Instance{room_instance_id: room_instance_id}} =
        Ally.update_meeting(
          owner_id,
          TestFactory.create_meeting_request("meeting_1", 4, start_time, end_time, 60),
          meeting_id
        )

      assert ^room_2 = Room.get(room_instance_id)
    end

    test "Can successfully update the timerange", %{
      room_1: room_1,
      room_2: room_2,
      owner_id: owner_id
    } do
      start_time = ~N[2020-01-01 14:00:00]
      end_time = ~N[2020-01-01 15:00:00]

      {:ok, %Meeting.Instance{id: meeting_id, room_instance_id: room_instance_id}} =
        Ally.Meeting.create(
          %{
            name: "meeting_1",
            range: Timestamp.Range.new(start_time, end_time),
            after_time: start_time,
            before_time: end_time,
            room_instance_id: room_1.id
          },
          TestFactory.participant_gen(3)
        )

      assert ^room_1 = Room.get(room_instance_id)

      start_time = ~N[2020-01-01 13:00:00]
      end_time = ~N[2020-01-01 14:00:00]

      # Book the same room one hour before
      {:ok, %Meeting.Instance{room_instance_id: room_instance_id}} =
        Ally.Meeting.create(
          %{
            name: "meeting_2",
            range: Timestamp.Range.new(start_time, end_time),
            after_time: start_time,
            before_time: end_time,
            room_instance_id: room_1.id
          },
          TestFactory.participant_gen(3)
        )

      assert ^room_1 = Room.get(room_instance_id)

      # Update the first meeting to the new range, with room1 already booked
      {:ok, %Meeting.Instance{room_instance_id: room_instance_id}} =
        Ally.update_meeting(
          owner_id,
          TestFactory.create_meeting_request("meeting_1", 3, start_time, end_time, 60),
          meeting_id
        )

      assert ^room_2 = Room.get(room_instance_id)
    end
  end

  # describe "Rule business rules after updates" do
  #   test "After a meeting is moved to a new time range business rules should be reallocated", %{
  #     owner_id: owner_id
  #   } do
  #     start_time = ~N[2020-01-01 14:00:00]
  #     end_time = ~N[2020-01-01 15:00:00]
  #     range = Timestamp.Range.new(start_time, end_time)

  #     {:ok, %Meeting.Instance{id: meeting_id, room_instance_id: room_instance_id}} =
  #       Ally.request_meeting(
  #         owner_id,
  #         TestFactory.create_meeting_request("meeting_1", 3, start_time, end_time)
  #       )

  #     {:ok, %Meeting.Instance{id: meeting_id2, room_instance_id: room_instance_id2}} =
  #       Ally.request_meeting(
  #         owner_id,
  #         TestFactory.create_meeting_request("meeting_2", 3, start_time, end_time)
  #       )

  #     {:ok, %Meeting.Instance{id: meeting_id2, room_instance_id: room_instance_id3}} =
  #       Ally.request_meeting(
  #         owner_id,
  #         TestFactory.create_meeting_request("meeting_2", 2, start_time, end_time)
  #       )

  #     booked_rooms = Ally.Filter.get_all_booked_rooms(owner_id, range)
  #     assert Enum.map(booked_rooms, & &1.size) == [3, 5, 6]
  #     assert %Room.Instance{size: 6} = Ally.Room.get(room_instance_id3)

  #     new_start_time = ~N[2020-01-01 18:00:00]
  #     new_end_time = ~N[2020-01-01 19:00:00]
  #     new_range = Timestamp.Range.new(start_time, end_time)

  #     {:ok, %Meeting.Instance{room_instance_id: room_instance_id}} =
  #       Ally.update_meeting(
  #         owner_id,
  #         TestFactory.create_meeting_request("meeting_1", 3, new_start_time, new_end_time),
  #         meeting_id
  #       )

  #     new_booked_rooms = Ally.Filter.get_all_booked_rooms(owner_id, range)
  #     assert Enum.map(new_booked_rooms, & &1.size) == [3, 5]

  #     # assert two participants meeting is now in smaller room
  #     assert %Room.Instance{size: 3} = Ally.Room.get(room_instance_id3)
  #   end

  # test "Meeting room are optimised if a smaller group ", %{
  #   owner_id: owner_id
  # } do
  #   start_time = ~N[2020-01-01 14:00:00]
  #   end_time = ~N[2020-01-01 15:00:00]
  #   range = Timestamp.Range.new(start_time, end_time)

  #   {:ok, %Meeting.Instance{id: meeting_id, room_instance_id: room_instance_id}} =
  #     Ally.request_meeting(
  #       owner_id,
  #       TestFactory.create_meeting_request("meeting_1", 3, start_time, end_time)
  #     )

  #   {:ok, %Meeting.Instance{id: meeting_id2, room_instance_id: room_instance_id2}} =
  #     Ally.request_meeting(
  #       owner_id,
  #       TestFactory.create_meeting_request("meeting_2", 2, start_time, end_time)
  #     )

  #   booked_rooms = Ally.Filter.get_all_booked_rooms(owner_id, range)
  #   assert Enum.map(booked_rooms, & &1.size) == [3, 5]

  #   # meeting with smaller number of participants has the smallest room
  #   assert %Room.Instance{size: 3} = Ally.Room.get(room_instance_id2)
  # end
  # end
end
