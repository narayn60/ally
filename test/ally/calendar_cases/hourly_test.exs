defmodule Ally.CalendarCases.HourlyTest do
  @moduledoc false

  use ExUnit.Case

  alias Ecto.Adapters.SQL.Sandbox

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(Ally.Repo)
  end

  describe "Allocates correctly to single hour" do
    test "Rejects user when room full" do
      user_id = TestFactory.create_owner()

      Enum.each(
        [
          %{size: 4, name: "room_1"},
          %{size: 5, name: "room_2"},
          %{size: 6, name: "room_3"}
        ],
        &Ally.Room.create(user_id, &1)
      )

      start_time = ~N[2020-01-01 14:00:00]
      end_time = ~N[2020-01-01 15:00:00]

      # Setup Meeting 1
      {:ok, %Ally.Meeting.Instance{room_instance_id: _room_1}} =
        Ally.request_meeting(
          user_id,
          TestFactory.create_meeting_request("meeting_1", 3, start_time, end_time, 60)
        )

      # Setup Meeting 2
      {:ok, %Ally.Meeting.Instance{room_instance_id: _room_2}} =
        Ally.request_meeting(
          user_id,
          TestFactory.create_meeting_request("meeting_2", 3, start_time, end_time, 60)
        )

      # Setup Meeting 3
      {:ok, %Ally.Meeting.Instance{room_instance_id: _room_3}} =
        Ally.request_meeting(
          user_id,
          TestFactory.create_meeting_request("meeting_3", 4, start_time, end_time, 60)
        )

      # Reject Meeting 4
      {:error, :no_rooms} =
        Ally.request_meeting(
          user_id,
          TestFactory.create_meeting_request("meeting_4", 1, start_time, end_time, 60)
        )
    end
  end
end
