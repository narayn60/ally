defmodule Ally.CalendarCases.EventDeletionTest do
  @moduledoc false

  use ExUnit.Case

  alias Ally.Meeting
  alias Ecto.Adapters.SQL.Sandbox

  setup [
    :connect_db,
    :create_owner
  ]

  describe "Event Deletion" do
    setup [
      :create_room
    ]

    test "Can successfully delete an event", %{owner_id: owner_id} do
      start_time = ~N[2020-01-01 14:00:00]
      end_time = ~N[2020-01-01 15:00:00]

      {:ok, %Meeting.Instance{id: meeting_id, room_instance_id: _room_1}} =
        Ally.request_meeting(
          owner_id,
          TestFactory.create_meeting_request("meeting_1", 3, start_time, end_time, 60)
        )

      # Delete the meeting
      assert {:ok, _} = Ally.Meeting.delete(meeting_id)
    end

    test "Deleting an event re-enables the room", %{owner_id: owner_id} do
      start_time = ~N[2020-01-01 14:00:00]
      end_time = ~N[2020-01-01 15:00:00]

      {:ok, %Meeting.Instance{id: meeting_id, room_instance_id: _room_1}} =
        Ally.request_meeting(
          owner_id,
          TestFactory.create_meeting_request("meeting_1", 3, start_time, end_time, 60)
        )

      new_meeting_request =
        TestFactory.create_meeting_request("meeting_2", 3, start_time, end_time, 60)

      # Can't book as no rooms
      assert {:error, :no_rooms} = Ally.request_meeting(owner_id, new_meeting_request)

      # Delete the meeting
      assert {:ok, _} = Meeting.delete(meeting_id)

      # Room should be available
      {:ok, %Meeting.Instance{room_instance_id: _room_1}} =
        Ally.request_meeting(owner_id, new_meeting_request)
    end
  end

  describe "Run business rules after event deletion" do
    setup [
      :create_rooms
    ]

    test "After event deletion rooms should be re-assigned", %{
      owner_id: owner_id
    } do
      start_time = ~N[2020-01-01 14:00:00]
      end_time = ~N[2020-01-01 15:00:00]
      range = Timestamp.Range.new(start_time, end_time)

      {:ok, %Meeting.Instance{id: _meeting_id1, room_instance_id: _id}} =
        Ally.request_meeting(
          owner_id,
          TestFactory.create_meeting_request("meeting_1", 2, start_time, end_time, 60)
        )

      {:ok, %Meeting.Instance{id: meeting_id2, room_instance_id: _id}} =
        Ally.request_meeting(
          owner_id,
          TestFactory.create_meeting_request("meeting_2", 4, start_time, end_time, 60)
        )

      {:ok, %Meeting.Instance{id: _meeting_id3, room_instance_id: _id}} =
        Ally.request_meeting(
          owner_id,
          TestFactory.create_meeting_request("meeting_3", 6, start_time, end_time, 60)
        )

      booked_rooms = Ally.Filter.get_all_booked_rooms(owner_id, range)
      assert Enum.map(booked_rooms, & &1.size) == [2, 4, 6]

      {:ok, _instance} = Ally.delete_meeting(owner_id, meeting_id2)

      updated_booked_rooms = Ally.Filter.get_all_booked_rooms(owner_id, range)
      assert Enum.map(updated_booked_rooms, & &1.size) == [2, 6]
    end
  end

  defp connect_db(_context) do
    :ok = Sandbox.checkout(Ally.Repo)
  end

  defp create_room(context) do
    owner_id = Map.get(context, :owner_id)
    Map.put(context, :room, do_create_rooms(owner_id))
  end

  defp create_owner(context) do
    owner_id = TestFactory.create_owner()

    Map.put(context, :owner_id, owner_id)
  end

  defp create_rooms(context) do
    owner_id = Map.get(context, :owner_id)
    room1 = do_create_rooms(owner_id, 2, "room1")
    room2 = do_create_rooms(owner_id, 4, "room2")
    room3 = do_create_rooms(owner_id, 6, "room2")
    Map.put(context, :rooms, [room1, room2, room3])
  end

  defp do_create_rooms(owner_id, size \\ 4, name \\ "room_1") do
    {:ok, room} = Ally.Room.create(owner_id, %{size: size, name: name})
    room
  end
end
