defmodule Ally.OwnerTest do
  @moduledoc false

  use ExUnit.Case

  alias Ecto.Adapters.SQL.Sandbox

  alias Ally.Owner
  alias Ally.Owner.Instance

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(Ally.Repo)
  end

  describe "create/2" do
    test "can successfully create an owner" do
      [email] = TestFactory.email_generator(1)

      assert {:ok, %Instance{email: ^email}} = Owner.create(%{email: email})
    end

    test "can't create owner with identical email" do
      [email] = TestFactory.email_generator(1)

      assert {:ok, %Instance{email: ^email}} = Owner.create(%{email: email})

      assert {:error,
              %Ecto.Changeset{
                constraints: [
                  %{
                    constraint: "owner_instances_email_index",
                    error_message: "has already been taken",
                    error_type: :unique,
                    field: :email,
                    match: :exact,
                    type: :unique
                  }
                ]
              }} = Owner.create(%{email: email})
    end
  end

  describe "delete/1" do
    test "can succesfully delete an owner" do
      [email] = TestFactory.email_generator(1)

      assert {:ok, %Instance{email: ^email, id: owner_id}} = Owner.create(%{email: email})

      assert {:ok, %Instance{email: ^email}} = Owner.delete(owner_id)
    end

    test "return error if attempting to delete owner that doesn't exist" do
      random_id = Ecto.UUID.generate()
      assert {:error, _} = Owner.delete(random_id)
    end
  end

  describe "get/1" do
    test "can succesfully get an owner" do
      [email] = TestFactory.email_generator(1)

      assert {:ok, %Instance{email: ^email, id: owner_id}} = Owner.create(%{email: email})

      assert %Instance{email: ^email} = Owner.get(owner_id)
    end

    test "returns nil if owner doesn't exist" do
      owner_id = Ecto.UUID.generate()

      assert nil === Owner.get(owner_id)
    end
  end
end
