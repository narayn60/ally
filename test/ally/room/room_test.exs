defmodule Ally.Room.RoomTest do
  @moduledoc false

  use ExUnit.Case

  alias Ecto.Adapters.SQL.Sandbox

  alias Ally.{
    Meeting,
    Room
  }

  alias Ally.Room.Instance

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(Ally.Repo)
  end

  setup do
    owner_id = TestFactory.create_owner()

    %{owner_id: owner_id}
  end

  @spec generate_room(owner_id :: Ally.Owner.owner_instance_id()) :: String.t()
  defp generate_room(owner_id) do
    room_name = Ecto.UUID.generate()
    room = %{size: 2, name: room_name}

    {:ok, %Instance{id: instance_id}} = Room.create(owner_id, room)

    instance_id
  end

  describe "CRUD operations" do
    test "create/2", %{owner_id: owner_id} do
      room_name = Ecto.UUID.generate()
      room = %{size: 2, name: room_name}

      assert {:ok, %Instance{name: ^room_name}} = Room.create(owner_id, room)
    end

    test "delete/1 returns {:ok, instance} with existing room", %{owner_id: owner_id} do
      # Create the room
      instance_id = generate_room(owner_id)

      # Delete the room
      assert {:ok, %Instance{id: ^instance_id}} = Room.delete(instance_id)
    end

    test "delete/1 returns {:error, String.t()} with non-existent room" do
      # Delete non-existing room
      instance_id = Ecto.UUID.generate()

      expected_error_message = "Instance with id #{instance_id} doesn't exist"
      assert {:error, ^expected_error_message} = Room.delete(instance_id)
    end

    test "delete/1 deleting a room deletes all meetings assigned to that room", %{
      owner_id: owner_id
    } do
      room_instance_id = generate_room(owner_id)

      start_time = ~N[2020-01-01 14:00:00]
      end_time = ~N[2020-01-01 15:00:00]
      meeting_range = Timestamp.Range.new(start_time, end_time)

      # Create a meeting
      assert {:ok, %Meeting.Instance{id: meeting_id, room_instance_id: ^room_instance_id}} =
               Meeting.create(
                 %{
                   name: Ecto.UUID.generate(),
                   range: meeting_range,
                   after_time: start_time,
                   before_time: end_time,
                   room_instance_id: room_instance_id
                 },
                 []
               )

      # Delete the room
      assert {:ok, %Instance{id: ^room_instance_id}} = Room.delete(room_instance_id)

      # Ensure that meeting is deleted
      assert nil == Meeting.get(meeting_id)
    end

    test "update/2 returns {:ok, instance} with updated instance", %{owner_id: owner_id} do
      instance_id = generate_room(owner_id)
      updated_room_name = Ecto.UUID.generate()

      updates = %{name: updated_room_name}

      assert {:ok, %Instance{id: ^instance_id, name: ^updated_room_name}} =
               Room.update(instance_id, updates)
    end

    test "updates/2 returns {:error, Ecto.Changeset.t()} when false params specified", %{
      owner_id: owner_id
    } do
      instance_id = generate_room(owner_id)

      # Normally a string
      updated_room_name = 1234

      assert {:error, %Ecto.Changeset{}} = Room.update(instance_id, %{name: updated_room_name})
    end
  end

  describe "get_all/1" do
    test "returns all rooms belonging to an owner", %{owner_id: owner_id} do
      room_ids = Enum.map(1..2, fn _ -> generate_room(owner_id) end)

      # Seperate owner room
      owner_2 = TestFactory.create_owner()
      owner_2_room_id = generate_room(owner_2)

      all_rooms = Room.get_all(owner_id)
      assert owner_2_room_id not in all_rooms

      assert room_ids === Enum.map(all_rooms, & &1.id)
    end
  end
end
