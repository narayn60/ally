defmodule Ally.EventTest do
  @moduledoc false

  use ExUnit.Case

  alias Ecto.Adapters.SQL.Sandbox

  alias Ally.{
    Event,
    Meeting
  }

  alias Ally.Event.Instance

  setup [
    :connect_db,
    :create_owner
  ]

  describe "create/2" do
    setup [
      :create_room
    ]

    test "can succesfully create an event", %{
      owner_id: owner_id
    } do
      user_id = TestFactory.create_user()
      meeting_id = create_meeting(owner_id)

      assert {:ok, %Instance{user_id: ^user_id, meeting_id: ^meeting_id}} =
               Event.create(user_id, meeting_id)
    end

    test "fails if user id doesn't exist", %{
      owner_id: owner_id
    } do
      user_id = Ecto.UUID.generate()
      meeting_id = create_meeting(owner_id)
      assert {:error, %Ecto.Changeset{valid?: false}} = Event.create(user_id, meeting_id)
    end
  end

  describe "get/1" do
    setup [
      :create_room
    ]

    test "successfully returns an event when it exists", %{owner_id: owner_id} do
      meeting_id = create_meeting(owner_id)
      user_id = TestFactory.create_user()

      {:ok, %Instance{id: event_id}} = Event.create(user_id, meeting_id)

      assert %Instance{id: ^event_id} = Event.get(event_id)
    end

    test "returns nil when event doesn't exist" do
      event_id = Ecto.UUID.generate()

      assert nil === Event.get(event_id)
    end
  end

  defp connect_db(_context) do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(Ally.Repo)
  end

  defp create_room(context) do
    owner_id = Map.get(context, :owner_id)
    Map.put(context, :room, do_create_rooms(owner_id))
  end

  defp create_owner(context) do
    owner_id = TestFactory.create_owner()

    Map.put(context, :owner_id, owner_id)
  end

  defp create_meeting(owner_id) do
    [start_time, end_time] = [~N[2020-01-01 14:00:00], ~N[2020-01-01 15:00:00]]

    # Meeting created
    # Can change this later, as will want the request meeting call to automatically
    # create the event objects
    {:ok, %Meeting.Instance{id: meeting_id}} =
      Ally.request_meeting(
        owner_id,
        TestFactory.create_meeting_request("meeting_1", 2, start_time, end_time, 60)
      )

    meeting_id
  end

  defp do_create_rooms(owner_id, size \\ 4, name \\ "room_1") do
    {:ok, room} = Ally.Room.create(owner_id, %{size: size, name: name})
    room
  end
end
