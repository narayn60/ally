defmodule Ally.UserTest do
  @moduledoc false

  use ExUnit.Case

  alias Ecto.Adapters.SQL.Sandbox

  alias Ally.User
  alias Ally.User.Instance

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(Ally.Repo)
  end

  describe "create/2" do
    test "can succesfully create a user" do
      [email] = TestFactory.email_generator(1)

      assert {:ok, %Instance{email: ^email}} =
               User.create(%{email: email, name: "test_user", password: "test_password"})
    end

    test "fails if user with email already exists" do
      [email] = TestFactory.email_generator(1)

      # Create first user
      assert {:ok, %Instance{email: ^email}} =
               User.create(%{email: email, name: "test_user", password: "test_password"})

      # Fails on creation of user with same email
      assert {:error, %Ecto.Changeset{errors: [email: {"has already been taken", _}]}} =
               User.create(%{email: email, name: "test_user", password: "test_password"})
    end
  end

  describe "get/1" do
    test "successfully returns a user when they exist" do
      user_id = TestFactory.create_user()

      assert %Instance{id: ^user_id} = User.get(user_id)
    end

    test "returns nil when user doesn't exist" do
      user_id = Ecto.UUID.generate()

      assert nil === User.get(user_id)
    end
  end
end
