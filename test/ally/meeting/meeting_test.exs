defmodule Ally.MeetingTest do
  @moduledoc false

  use ExUnit.Case

  alias Ecto.Adapters.SQL.Sandbox

  alias Ally.{
    Meeting,
    Repo,
    User
  }

  alias Ally.Event.Instance, as: EventInstance
  alias Ally.Meeting.Instance, as: MeetingInstance
  alias Ally.Room.Instance, as: RoomInstance
  alias Ally.User.Instance, as: UserInstance

  setup [
    :connect_db,
    :create_owner
  ]

  describe "create/2" do
    setup [
      :create_room
    ]

    test "During successfull creation, corresponding events are created", %{
      room: %RoomInstance{id: room_instance_id}
    } do
      user_id = TestFactory.create_user()
      %UserInstance{email: user_email} = User.get(user_id)

      [start_time, end_time] = [~N[2020-01-01 14:00:00], ~N[2020-01-01 15:00:00]]
      meeting_range = Timestamp.Range.new(start_time, end_time)
      user_emails = [user_email]

      {:ok, %MeetingInstance{id: meeting_id}} =
        Meeting.create(
          %{
            name: "test meeting",
            range: meeting_range,
            after_time: start_time,
            before_time: end_time,
            room_instance_id: room_instance_id
          },
          user_emails
        )

      assert [%EventInstance{user_id: ^user_id, attending: true, meeting_id: ^meeting_id}] =
               Repo.all(EventInstance)
    end
  end

  describe "get_participants_emails/1" do
    setup [
      :create_room
    ]

    test "successfully returns emails of users attatched to the event", %{
      room: %RoomInstance{id: room_instance_id}
    } do
      user_ids = Enum.map(1..2, fn _ -> TestFactory.create_user() end)

      user_emails =
        Enum.map(user_ids, fn user_id ->
          %UserInstance{email: email} = User.get(user_id)
          email
        end)

      [start_time, end_time] = [~N[2020-01-01 14:00:00], ~N[2020-01-01 15:00:00]]
      meeting_range = Timestamp.Range.new(start_time, end_time)

      {:ok, %MeetingInstance{id: meeting_id}} =
        Meeting.create(
          %{
            name: "test meeting",
            range: meeting_range,
            after_time: start_time,
            before_time: end_time,
            room_instance_id: room_instance_id
          },
          user_emails
        )

      assert user_emails == Meeting.get_participants_emails(meeting_id)
    end
  end

  describe "get_in_range_order_by_participants/2" do
    test "Successfully returns rooms", %{
      owner_id: owner_id
    } do
      [start_time, end_time] = [~N[2020-01-01 14:00:00], ~N[2020-01-01 15:00:00]]
      meeting_range = Timestamp.Range.new(start_time, end_time)
      {:ok, postgrex_range} = Timestamp.Range.dump(meeting_range)

      # Create rooms
      [room_1, room_2, room_3] = Enum.map(1..3, &do_create_rooms(owner_id, 4, "room_#{&1}"))

      # Create users
      [email_1, email_2, email_3] =
        Enum.map(1..3, fn _ ->
          %UserInstance{email: email} = User.get(TestFactory.create_user())

          email
        end)

      # Create meetings
      Enum.each(
        [
          {room_1, [email_1]},
          {room_2, [email_1, email_2, email_3]},
          {room_3, [email_2, email_3]}
        ],
        fn {%RoomInstance{id: room_instance_id, name: name}, user_emails} ->
          Meeting.create(
            %{
              name: "meeting_#{name}",
              range: meeting_range,
              after_time: start_time,
              before_time: end_time,
              room_instance_id: room_instance_id
            },
            user_emails
          )
        end
      )

      # Create a meeting out of range to ensure it's not returned
      [after_time, before_time] = [~N[2020-01-01 16:00:00], ~N[2020-01-01 17:00:00]]

      Meeting.create(
        %{
          name: "meeting_invalid",
          range: Timestamp.Range.new(after_time, before_time),
          after_time: after_time,
          before_time: before_time,
          room_instance_id: room_1.id
        },
        [email_1]
      )

      # Ensure meetings are returned in ascending order of participants
      assert [
               %MeetingInstance{name: "meeting_room_1"},
               %MeetingInstance{name: "meeting_room_3"},
               %MeetingInstance{name: "meeting_room_2"}
             ] = Meeting.get_in_range_ordered_by_participants(owner_id, postgrex_range)
    end
  end

  defp connect_db(_context) do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(Ally.Repo)
  end

  defp create_room(context) do
    owner_id = Map.get(context, :owner_id)
    Map.put(context, :room, do_create_rooms(owner_id))
  end

  defp create_owner(context) do
    owner_id = TestFactory.create_owner()

    Map.put(context, :owner_id, owner_id)
  end

  defp do_create_rooms(owner_id, size \\ 4, name \\ "room_1") do
    {:ok, room} = Ally.Room.create(owner_id, %{size: size, name: name})
    room
  end
end
