defmodule Ally.FilterTest do
  @moduledoc false

  use ExUnit.Case

  alias Ally.Filter
  alias Ecto.Adapters.SQL.Sandbox

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(Ally.Repo)
  end

  describe "find_available_rooms/5" do
    test "Given a request, returns all available rooms with available_times" do
      owner_id = TestFactory.create_owner()

      business_start_time = %{
        hour: 9,
        minute: 0
      }

      business_end_time = %{
        hour: 17,
        minute: 0
      }

      # Create meeting rooms
      [_room_1_id, room_2_id, room_3_id] =
        Enum.map(
          [
            %{size: 4, name: "room_1"},
            %{size: 5, name: "room_2"},
            %{size: 6, name: "room_3"}
          ],
          fn params ->
            {:ok, %{id: id}} = Ally.Room.create(owner_id, params)
            id
          end
        )

      Enum.each(
        [
          {%{
             name: "meeting_1",
             range: Timestamp.Range.new(~N[2020-01-01 09:00:00], ~N[2020-01-01 10:00:00]),
             after_time: ~N[2020-01-01 09:00:00],
             before_time: ~N[2020-01-01 10:00:00],
             room_instance_id: room_2_id
           }, TestFactory.participant_gen(5)},
          {%{
             name: "meeting_2",
             range: Timestamp.Range.new(~N[2020-01-01 11:00:00], ~N[2020-01-01 14:00:00]),
             after_time: ~N[2020-01-01 11:00:00],
             before_time: ~N[2020-01-01 14:00:00],
             room_instance_id: room_2_id
           }, TestFactory.participant_gen(5)},
          {%{
             name: "meeting_3",
             range: Timestamp.Range.new(~N[2020-01-01 14:00:00], ~N[2020-01-01 15:00:00]),
             after_time: ~N[2020-01-01 14:00:00],
             before_time: ~N[2020-01-01 15:00:00],
             room_instance_id: room_3_id
           }, TestFactory.participant_gen(6)},
          {%{
             name: "meeting_4",
             range: Timestamp.Range.new(~N[2020-01-02 10:00:00], ~N[2020-01-02 11:00:00]),
             after_time: ~N[2020-01-02 10:00:00],
             before_time: ~N[2020-01-02 11:00:00],
             room_instance_id: room_2_id
           }, TestFactory.participant_gen(5)},
          {%{
             name: "meeting_5",
             range: Timestamp.Range.new(~N[2020-01-02 12:00:00], ~N[2020-01-02 13:00:00]),
             after_time: ~N[2020-01-02 12:00:00],
             before_time: ~N[2020-01-02 13:00:00],
             room_instance_id: room_2_id
           }, TestFactory.participant_gen(5)}
        ],
        fn {meeting_attrs, participants} ->
          Ally.Meeting.create(meeting_attrs, participants)
        end
      )

      expected_result = %{
        room_2_id => [
          Timestamp.Range.new(~N[2020-01-01 14:00:00.000000], ~N[2020-01-01 17:00:00.000000]),
          Timestamp.Range.new(~N[2020-01-02 13:00:00.000000], ~N[2020-01-02 17:00:00.000000]),
          Timestamp.Range.new(~N[2020-01-03 09:00:00.000000], ~N[2020-01-03 17:00:00.000000])
        ],
        room_3_id => [
          Timestamp.Range.new(~N[2020-01-01 10:00:00.000000], ~N[2020-01-01 14:00:00.000000]),
          Timestamp.Range.new(~N[2020-01-01 15:00:00.000000], ~N[2020-01-01 17:00:00.000000]),
          Timestamp.Range.new(~N[2020-01-02 09:00:00.000000], ~N[2020-01-02 17:00:00.000000]),
          Timestamp.Range.new(~N[2020-01-03 09:00:00.000000], ~N[2020-01-03 17:00:00.000000])
        ]
      }

      assert ^expected_result =
               owner_id
               |> Filter.find_available_rooms(
                 Timestamp.Range.new(~N[2020-01-01 10:00:00], ~N[2020-01-03 19:00:00]),
                 5,
                 120 * 60,
                 business_start_time,
                 business_end_time
               )
               |> Ally.Repo.all()
               |> Enum.group_by(& &1.room_id, fn entry ->
                 {:ok, range} = Timestamp.Range.load(entry.time_range)
                 range
               end)
    end
  end
end
