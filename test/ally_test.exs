defmodule AllyTest do
  @moduledoc false

  use ExUnit.Case

  alias Ally.Meeting.{
    Instance,
    Request
  }

  alias Ecto.Adapters.SQL.Sandbox

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(Ally.Repo)
  end

  describe "request_meeting/2" do
    test "Given a request, returns all available rooms with available_times" do
      owner_id = TestFactory.create_owner()

      # Create meeting rooms
      [_room_1_id, room_2_id, room_3_id] =
        Enum.map(
          [
            %{size: 4, name: "room_1"},
            %{size: 5, name: "room_2"},
            %{size: 6, name: "room_3"}
          ],
          fn params ->
            {:ok, %{id: id}} = Ally.Room.create(owner_id, params)
            id
          end
        )

      Enum.each(
        [
          {%{
             name: "meeting_1",
             range: Timestamp.Range.new(~N[2020-01-01 09:00:00], ~N[2020-01-01 10:00:00]),
             after_time: ~N[2020-01-01 09:00:00],
             before_time: ~N[2020-01-01 10:00:00],
             room_instance_id: room_2_id
           }, TestFactory.participant_gen(5)},
          {%{
             name: "meeting_2",
             range: Timestamp.Range.new(~N[2020-01-01 11:00:00], ~N[2020-01-01 14:00:00]),
             after_time: ~N[2020-01-01 11:00:00],
             before_time: ~N[2020-01-01 14:00:00],
             room_instance_id: room_2_id
           }, TestFactory.participant_gen(5)},
          {%{
             name: "meeting_3",
             range: Timestamp.Range.new(~N[2020-01-01 14:00:00], ~N[2020-01-01 15:00:00]),
             after_time: ~N[2020-01-01 14:00:00],
             before_time: ~N[2020-01-01 15:00:00],
             room_instance_id: room_3_id
           }, TestFactory.participant_gen(6)},
          {%{
             name: "meeting_4",
             range: Timestamp.Range.new(~N[2020-01-02 10:00:00], ~N[2020-01-02 11:00:00]),
             after_time: ~N[2020-01-02 10:00:00],
             before_time: ~N[2020-01-02 11:00:00],
             room_instance_id: room_2_id
           }, TestFactory.participant_gen(5)},
          {%{
             name: "meeting_5",
             range: Timestamp.Range.new(~N[2020-01-02 12:00:00], ~N[2020-01-02 13:00:00]),
             after_time: ~N[2020-01-02 12:00:00],
             before_time: ~N[2020-01-02 13:00:00],
             room_instance_id: room_2_id
           }, TestFactory.participant_gen(5)}
        ],
        fn {meeting_attrs, participants} ->
          Ally.Meeting.create(meeting_attrs, participants)
        end
      )

      # Meeting longer than work day
      assert {:error, :no_rooms} =
               Ally.request_meeting(
                 owner_id,
                 %Request{
                   name: "test_meeting",
                   after_time: ~N[2020-01-01 09:00:00],
                   before_time: ~N[2020-01-05 10:00:00],
                   duration_in_minutes: 900,
                   user_emails: TestFactory.participant_gen(5)
                 }
               )

      assert {:ok, %Instance{name: "test_meeting"}} =
               Ally.request_meeting(
                 owner_id,
                 %Request{
                   name: "test_meeting",
                   after_time: ~N[2020-01-01 09:00:00],
                   before_time: ~N[2020-01-05 10:00:00],
                   duration_in_minutes: 300,
                   user_emails: TestFactory.participant_gen(5)
                 }
               )
    end
  end
end
