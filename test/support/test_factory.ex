defmodule TestFactory do
  @moduledoc false

  alias Ally.User.Instance, as: UserInstance

  require ExUnitProperties

  @spec create_user :: String.t()
  def create_user do
    [user_email] = email_generator(1)

    {:ok, %UserInstance{id: user_id}} =
      Ally.User.create(%{
        name: "test_name",
        email: user_email,
        password: "test_password"
      })

    user_id
  end

  @spec create_owner :: String.t()
  def create_owner do
    [user_email] = email_generator(1)

    {:ok, %Ally.Owner.Instance{id: owner_id}} = Ally.Owner.create(%{email: user_email})

    owner_id
  end

  @spec create_meeting_request(
          name :: String.t(),
          no_of_participants :: number(),
          start_time :: NaiveDateTime.t(),
          end_time :: NaiveDateTime.t(),
          duration_in_minutes :: integer()
        ) ::
          %Ally.Meeting.Request{}
  def create_meeting_request(name, no_of_participants, start_time, end_time, duration_in_minutes) do
    %Ally.Meeting.Request{
      name: name,
      user_emails: participant_gen(no_of_participants),
      after_time: start_time,
      before_time: end_time,
      duration_in_minutes: duration_in_minutes
    }
  end

  @spec participant_gen(no_of_participants :: number()) :: [UserInstance.email()]
  def participant_gen(no_of_participants) do
    Enum.map(1..no_of_participants, fn _ ->
      [user_email] = email_generator(1)

      {:ok, %UserInstance{id: _user_id}} =
        Ally.User.create(%{
          name: "test_name",
          email: user_email,
          password: "test_password"
        })

      user_email
    end)
  end

  @spec email_generator(no_of_emails :: number()) :: [String.t()]
  def email_generator(no_of_emails) do
    domains = [
      "gmail.com",
      "hotmail.com",
      "yahoo.com"
    ]

    stream_generator =
      ExUnitProperties.gen all name <- StreamData.string(:alphanumeric),
                               name != "",
                               domain <- StreamData.member_of(domains) do
        name <> "@" <> domain
      end

    Enum.take(StreamData.resize(stream_generator, 20), no_of_emails)
  end
end
