# Template

To start your Phoenix server:

  * Setup the project with `mix setup`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

# Typecheck 

To run a typecheck run

  * `mix dialyzer`

# Linting

To run linter run

  * `mix credo --strict`

# Security Audit

To run a security check run

  * `mix sobelow`
