defmodule Ally.Repo.Migrations.CreateAlly.Meeting.Instances do
  use Ecto.Migration

  def change do
    create table(:meeting_instances, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :name, :string

      add :range, :tsrange, null: false
      add :before_time, :naive_datetime, null: false
      add :after_time, :naive_datetime, null: false

      add :room_instance_id, references(:room_instances, type: :uuid, on_delete: :delete_all),
        null: false

      timestamps()
    end
  end
end
