defmodule Ally.Repo.Migrations.CreateAlly.Owner.Instances do
  use Ecto.Migration

  def change do
    create table(:owner_instances, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :email, :string

      timestamps()
    end

    create unique_index("owner_instances", [:email])
  end
end
