defmodule Ally.Repo.Migrations.CreateEventInstances do
  use Ecto.Migration

  def change do
    create table(:event_instances, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :attending, :boolean, default: true

      add :user_id, references(:user_instances, type: :uuid, on_delete: :delete_all), null: false

      add :meeting_id, references(:meeting_instances, type: :uuid, on_delete: :delete_all),
        null: false

      timestamps default: fragment("NOW()")
    end
  end
end
