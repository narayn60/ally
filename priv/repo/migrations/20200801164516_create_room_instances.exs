defmodule Ally.Repo.Migrations.CreateAlly.Room.Instances do
  use Ecto.Migration

  def change do
    create table(:room_instances, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :size, :integer
      add :name, :string

      add :owner_instance_id, references(:owner_instances, type: :uuid, on_delete: :delete_all),
        null: false

      timestamps()
    end
  end
end
