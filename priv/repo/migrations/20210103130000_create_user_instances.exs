defmodule Ally.Repo.Migrations.CreateUserInstances do
  use Ecto.Migration

  def change do
    create table(:user_instances, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :name, :string
      add :email, :string
      add :password, :string

      timestamps()
    end

    create unique_index("user_instances", [:email])
  end
end
